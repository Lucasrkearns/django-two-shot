from django.shortcuts import render, redirect, get_list_or_404
from receipts.models import ExpenseCategory, Account, Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseForm, AccountForm
# Create your views here.

@login_required
def home(request):
    all_receipts =  Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": all_receipts,
    }
    return render(request, "receipts/home.html", context)


@login_required
def create(request):
    if request.method == "POST":

        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    all_categories =  ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": all_categories,
    }
    return render(request, "receipts/categories.html", context)


@login_required
def account_list(request):
    all_accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": all_accounts
    }
    return render(request, "receipts/accounts.html", context)

@login_required
def create_category(request):
    if request.method == "POST":

        form = ExpenseForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseForm()

    context = {
        "form": form
    }
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":

        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {
        "form": form
    }
    return render(request, "receipts/create_account.html", context)
